export class Payment {
  id?: number;
  userID?: number;
  eventID?: number;
  offerTypeID?: number;
  amount?: number;
  paymentDateTime?: string;
}
