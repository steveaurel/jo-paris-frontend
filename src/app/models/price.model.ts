import { OfferType } from './offer-type.model';

export class Price {
  id?: number;
  amount?: number;
  currency?: string;
  offerType?: OfferType;

}
