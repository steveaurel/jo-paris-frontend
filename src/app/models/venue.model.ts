import { Location } from './location.model';

export class Venue {
  id?: number;
  name?: string;
  capacity?: number;
  location?: Location;
}
